package cn.uncode.springcloud.gateway.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import cn.uncode.springcloud.gateway.canary.StrategyModel;

import lombok.Data;


/**
 *
 * 
 * @author juny
 * @date 2019年1月18日
 *
 */
@Data
@ConfigurationProperties(prefix = "info.canary", ignoreInvalidFields = true)
public class CanaryProperties {
	
	private boolean enabled = true;
	
	private List<StrategyModel> strategy;

}
