package cn.uncode.springcloud.starter.log.annotation;

import java.util.HashMap;
import java.util.Map;

public enum OperationType{
	
	SEARCH(1,"查询"),
	CREATE(2,"创建"),
	UPDATE(3,"更新"),
	DELETE(4,"删除"),
	/* *
	 * 复杂功能
	 */
	SPECIAL(5,"特殊操作");

	public final int type;
	public String name;

	OperationType(int type, String name) {
		this.type = type;
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	public int getType() {
		return this.type;
	}

	public static Map<Integer, OperationType> map = new HashMap<>();

	static {
		for (OperationType value: OperationType.values()) {
			map.put(value.type, value);
		}
	}
}
